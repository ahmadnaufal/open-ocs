<!DOCTYPE html>
<html lang="en" ng-app="sessionApp">
<head>
	<title> <?= $title ?> | Telkom OCS</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?= base_url('assets/css/materialize.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/materialize.datatables.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <?= link_tag( "https://fonts.googleapis.com/icon?family=Material+Icons" ) ?>
</head>
<body>
