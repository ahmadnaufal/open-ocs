
    <script>
        var base_url = "<?= site_url() ?>";
    </script>
	<script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/materialize.min.js') ?>"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-resource.js"></script>

    <script type="text/javascript" src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/materialize.datatables.js') ?>"></script>
    
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.mask.min.js') ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-materialize/0.2.1/angular-materialize.min.js"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/app.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/main.js') ?>"></script>
</body>
</html>
