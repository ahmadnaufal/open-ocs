<header>
    <!-- NAVBAR AREA -->
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <a href="#" data-activates="side-menu-nav" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only"><i class="material-icons">menu</i></a>
                <a href="<?= site_url() ?>" class="navbar-logo">Online Charging System</a>
                <ul id="nav-mobile" class="right">
                    <li>Hello, <b><?= $this->session->userdata('logged_in')['username'] ?></b></li>
                    <li><a href="<?= site_url('users/signOut') ?>"><i class="material-icons left">exit_to_app</i>Logout</a></li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- SIDEBAR AREA -->
    <ul id="side-menu-nav" class="side-nav fixed">
        <li><a class="subheader">Dashboards</a></li>
        <li><a href="<?= site_url('number/history') ?>"><i class="material-icons">view_list</i>History</a></li>
        <li><a href="<?= site_url('number/billing') ?>"><i class="material-icons">perm_contact_calendar</i>Billing History</a></li>
        <li><div class="divider"></div></li>
        <?php if ($this->session->userdata('logged_in')['is_admin'] == USER_ROLE_ADMIN) : ?>
            <li><a class="subheader">Management</a></li>
            <li><a href="<?= site_url('users/listusers') ?>"><i class="material-icons">supervisor_account</i>User Management</a></li>
            <li><a href="<?= site_url('users/listnumbers') ?>"><i class="material-icons">settings_phone</i>Number Management</a></li>
        <?php endif; ?>
    </ul>
    
</header>
