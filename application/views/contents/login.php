<main class="login-page">
	<div class="container">
		<div class="row">
			<div class="col m6 offset-m3">
				<form action="<?= site_url('auth/authorize') ?>" class="form login vertical-center" method="POST">
					<h4 class="center">Online Charging System</h4>
					<p class="form-helper center">Silakan login terlebih dahulu</p>
					<?php if ($error = $this->session->flashdata('error')) : ?>
						<p><?php echo $error; ?></p>
					<?php endif; ?>
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">account_circle</i>
							<input type="text" class="validate" id="username" name="username">
							<label for="username">Username</label>
						</div>
						<div class="input-field col s12">
							<i class="material-icons prefix">https</i>
							<input type="password" class="validate" id="password" name="password">
							<label for="password">Password</label>
						</div>
					</div>
					<button class="btn waves-effect waves-light full-width" type="submit" name="action">
						Submit <i class="material-icons right">send</i>
					</button>
				</form>
			</div>
		</div>
	</div>
</main>
