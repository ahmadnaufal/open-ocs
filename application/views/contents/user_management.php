<main class="sidebar-padding-left">
    <div class="container-mini">
        <?php if ($this->session->flashdata('success')) : ?>

            <div class="alert alert-success" role="alert">
                <?= $this->session->flashdata('success') ?>
            </div>

        <?php elseif ($this->session->flashdata('error')) : ?>

            <div class="alert alert-danger" role="alert">
                <?= $this->session->flashdata('error') ?>
            </div>

        <?php endif; ?>
        <div class="card material-table">
            <div class="table-header">
                <span class="table-title">User Management</span>
                <div class="actions">
                    <a href="#add_users" class="waves-effect btn-flat nopadding modal-trigger" id="add_user"><i class="material-icons">person_add</i></a>
                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                </div>
            </div>
            <table class="striped" id="myDataTableUser">
                <?php $i = 0; ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Customer Name</th>
                        <th>Role</th>
                        <th>Tariff</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                                
                        <tr id="user_<?= $user['id'] ?>">
                            <td><?= ++$i ?></td>
                            <td class="user-username"><?= $user['username'] ?></td>
                            <td class="user-customername"><?= $user['full_name'] ?></td>
                            <td class="user-role"><?= ($user['is_admin'] == 1) ? "Admin" : "User" ?></td>
                            <td class="user-tariff"><?= $user['tariff'] ?></td>
                            <td>
                                <a href="" class="edit-user" data-id="<?= $user['id'] ?>"><i class="material-icons">mode_edit</i></a>
                                <input type="hidden" value="<?= $user['id'] ?>" name="id">
                                <a href="<?= base_url('users/deleteuser/'.$user['id']) ?>" onclick="return confirm('Delete this user?')"><i class="material-icons">delete</i></button>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php $this->load->view('modals/add_users'); ?>

</main>


