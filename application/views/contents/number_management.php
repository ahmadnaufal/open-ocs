<main class="sidebar-padding-left">
    <div class="container-mini">
        <?php if ($this->session->flashdata('success')) : ?>

            <div class="alert alert-success" role="alert">
                <?= $this->session->flashdata('success') ?>
            </div>

        <?php elseif ($this->session->flashdata('error')) : ?>

            <div class="alert alert-danger" role="alert">
                <?= $this->session->flashdata('error') ?>
            </div>

        <?php endif; ?>
        <div class="card material-table">
            <div class="table-header">
                <span class="table-title">Number Management</span>
                <div class="actions">
                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                </div>
            </div>
            <table class="striped" id="myDataTableNumber">
                <?php $i = 0; ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Assigned Number</th>
                        <th>Number List</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($numbers as $number) : ?>
                                
                        <tr id="user_<?= $number['id'] ?>">
                            <td><?= ++$i ?></td>
                            <td class="user-username"><?= $number['username'] ?></td>
                            <td><?= sizeof($number['numbers']) ?></td>
                            <td class="number-list">
                                <?php foreach ($number['numbers'] as $num) {
                                    echo $num['id'] . "<br>";
                                } ?>
                            </td>
                            <td>
                                <a href="#edit_numbers" class="edit-number modal-trigger" data-id="<?= $number['id'] ?>"><i class="material-icons">mode_edit</i></a>
                                <input type="hidden" value="<?= $number['id'] ?>" name="id">
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php $this->load->view('modals/edit_numbers'); ?>

</main>
