<?php $userdata = $this->session->userdata('logged_in'); ?>

<main class="sidebar-padding-left" ng-controller="SessionBillingController">
    <div class="container-mini">
        <h3>Billing</h3>
        <form action="" id="billing_form" class="">
            <div class="row">
                <div class="col m6">
                    <div class="input-field col s12">
                        <?php if ($this->session->userdata('logged_in')['is_admin'] == USER_ROLE_ADMIN) : ?>
                            <input type="text" id="account_number" name="account_number" class="validate" ng-model="account">
                        <?php else : ?>
                            <select name="account_number" id="account_number" ng-model="selected_number" material-select>
                                <?php foreach ($numbers as $number) : ?>
                                    <option value="<?= $number['id'] ?>"><?= $number['id'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <label for="account_number">Account Number (Example: 6222xxxxxxxx)</label>
                    </div>
                </div>
                <div class="col m6">
                    <div class="row">
                        <div class="input-field col s8">
                            <input input-date
                                type="text"
                                name="start_date"
                                id="start_date"
                                ng-model="date_start"
                                container=""
                                format="yyyy-mm-dd"
                                months-full="{{ month }}"
                                months-short="{{ monthShort }}"
                                weekdays-full="{{ weekdaysFull }}"
                                weekdays-short="{{ weekdaysShort }}"
                                weekdays-letter="{{ weekdaysLetter }}"
                                today="today"
                                first-day="1"
                                clear="clear"
                                close="close"
                                select-years="15" />
                            <label for="start_date">Start Time</label>
                        </div>
                        <div class="input-field col s4">
                            <input type="text" name="start_time" id="start_time" class="timepick-mask" ng-model="time_start" placeholder="Today">
                            <label for="start_time">HH:MM (Def: Now)</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8">
                            <input input-date
                                type="text"
                                name="end_date"
                                id="end_date"
                                ng-model="date_end"
                                container=""
                                format="yyyy-mm-dd"
                                months-full="{{ month }}"
                                months-short="{{ monthShort }}"
                                weekdays-full="{{ weekdaysFull }}"
                                weekdays-short="{{ weekdaysShort }}"
                                weekdays-letter="{{ weekdaysLetter }}"
                                today="today"
                                first-day="1"
                                clear="clear"
                                close="close"
                                select-years="15" />
                            <label for="end_date">End Time</label>
                        </div>
                        <div class="input-field col s4">
                            <input type="text" name="end_time" id="end_time" class="timepick-mask" ng-model="time_end" placeholder="Today">
                            <label for="end_time">HH:MM (Def: Now)</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" ng-click="change_state()" type="button">Submit</button>
                </div>
            </div>
        </form>
        <div class="card material-table">
            <table class="striped" id="billingTable">
                <thead>
                    <tr>
                        <th width="17%">Calling Party</th>
                        <th width="17%">Called Party</th>
                        <th width="18%">Call Start</th>
                        <th width="18%">Call End</th>
                        <th width="10%">Duration</th>
                        <th width="10%">Tariff</th>
                        <th width="10%">Total Billing</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="session in sessions" ng-class="{ongoing: (session.end === null)}">
                        <td>{{session.calling}}</td>
                        <td>{{session.called}}</td>
                        <td>{{session.start}}</td>
                        <td>{{session.end}}</td>
                        <td>{{session.duration}}</td>
                        <td>{{session.tariff}}</td>
                        <td>{{session.total_tariff}}</td>
                    </tr>
                </tbody>
            </table>
            <div class="table-footer">
                Total Number of Row: <span id="row_number" ng-model="row_number">{{row_number}}</span>
            </div>
        </div>
    </div>
</main>

<script>
    var history_url = "<?= site_url() ?>number/gethistory";
</script>
