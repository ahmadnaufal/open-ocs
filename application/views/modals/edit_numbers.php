<div id="edit_numbers" class="modal">
    <?php $attrs = array('id' => 'user_form'); ?>
    <?= form_open_multipart('users/editnumber', $attrs) ?>
        <div class="modal-content row">
            <h4 class="modal-title">Add/Edit User Number</h4>
<!--             <div class="row">
                <div class="col s6">
                    <div class="input-field col s12">
                        <input type="text" id="username" name="username" class="validate" value="" disabled>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <!-- <div class="col s6">
                    <p>
                        <input class="with-gap" name="method" type="radio" id="method_file_upload" value="file">
                        <label for="method_file_upload">File Upload</label>
                    </p>
                    <div class="input-field">
                        <input type="file" id="number_file" name="number_file" class="validate">
                    </div>
                </div> -->
                <div class="col s12">
                    <p>
                        <input class="with-gap" name="method" type="radio" id="method_manual_input" value="manual" checked>
                        <label for="method_manual_input">Manual Input</label>
                    </p>
                    <input type="hidden" id="numberCount" name="numberCount" value="1">
                    <div id="numberForm" class="row">
                        <!-- <div id="numbergroup_1">
                            <div class="col s8">
                                <div class="input-field">
                                    <input type="text" id="number_1" name="number_1" class="validate">
                                    <label for="number_1">Number 1</label>
                                </div>
                            </div>
                            <div class="col s4">
                                <div class="pull-right">
                                    <button class="btn" type="button" onclick="removeNumber()">-</button>
                                    <button class="btn" type="button" onclick="addNumber()">+</button>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="username" name="username" value="">
            <input type="hidden" id="user_id" name="id" value="">
            <button type="submit" class=" modal-action waves-effect waves-green btn-flat">Submit</button>
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
        </div>
    </form>
</div>
