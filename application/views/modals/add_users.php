<div id="add_users" class="modal">
    <?php $attrs = array('id' => 'user_form'); ?>
    <?= form_open('users/addnewuser', $attrs) ?>
        <div class="modal-content row">
            <h4 class="modal-title">Add New User</h4>
            <div class="col s6">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="username" name="username" class="validate">
                        <label for="username">Username</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="full_name" name="full_name" class="validate">
                        <label for="full_name">Customer Name</label>
                    </div>
                </div>
            </div>
            <div class="col s6">
                <div class="row">
                    <div class="input-field col m7">
                        <input type="password" id="password" name="password" class="validate">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col m5">
                       <select name="is_admin" id="is_admin">
                            <option value="1">Admin</option>
                            <option value="0" selected>User</option>
                        </select>
                        <label for="is_admin">User Role</label>
                    </div>
                    <div class="input-field col m7">
                        <input type="number" id="tariff" name="tariff" class="validate">
                        <label for="tariff">Tariff</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="user_id" name="id" value="">
            <input type="hidden" id="old_username" name="old_username" value="">
            <button type="submit" class=" modal-action waves-effect waves-green btn-flat">Submit</button>
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
        </div>
    </form>
</div>
