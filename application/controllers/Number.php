<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Number extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Number_model', 'number');
    }

    public function history() {
        $userdata = $this->session->userdata('logged_in');
        if (!isset($userdata)) {
            redirect('users');
        } else {
            if ($userdata['is_admin'] != USER_ROLE_ADMIN) {
                $numbers = json_decode($this->number->get_number_from_user($userdata['token']), true);
                $data['numbers'] = $numbers;
            }
            $data['title'] = "History";
            $this->load->view('templates/html.php', $data);
            $this->load->view('templates/header.php');
            $this->load->view('contents/history.php', $data);
            $this->load->view('templates/footer.php');
        }

    }

    public function billing() {
        $userdata = $this->session->userdata('logged_in');
        if (!isset($userdata)) {
            redirect('users');
        } else {
            if ($userdata['is_admin'] != USER_ROLE_ADMIN) {
                $numbers = json_decode($this->number->get_number_from_user($userdata['token']), true);
                $data['numbers'] = $numbers;
            }
            $data['title'] = "Billing";
            $this->load->view('templates/html.php', $data);
            $this->load->view('templates/header.php');
            $this->load->view('contents/billing.php');
            $this->load->view('templates/footer.php');
        }

    }

    public function getHistory() {
        $this->load->model('history_model');

        $account = $this->input->get("account");
        if ($account[0] == "+") {
            $account = substr($account, 1);
        }

        $request_data = array(
            'account'   => $account,
            'type'      => $this->input->get("type"),
            'start'     => $this->input->get("start"),
            'end'       => $this->input->get("end"),
        );

        $result = $this->history_model->get_all_history($request_data);
        echo $result;
    }

    public function listNumbers() {
        $userdata = $this->session->userdata('logged_in');

        if (!isset($userdata)) {
            redirect('users');
        } else {
            $this->load->model('Users_model', 'users');
            $data['title'] = "Number Management";

            $user_data = $this->users->get_all_users();
            $numbers_data['numbers'] = array();
            for ($i=0; $i < count($user_data); $i++) { 
                $user['id'] = $user_data[$i]['id'];
                $user['username'] = $user_data[$i]['username'];
                $user['numbers'] = $this->number->get_all_numbers($user['username']);
                $numbers_data['numbers'][] = $user;
            }

            $this->load->view('templates/html.php', $data);
            $this->load->view('templates/header.php');
            $this->load->view('contents/number_management.php', $numbers_data);
            $this->load->view('templates/footer.php');
        }
    }

    public function addNumber() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('id', 'Account', 'trim|required|xss_clean');

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
        } else {
            $username = $this->input->post("username");
            $id = $this->input->get("id");
            
            $account_res = json_decode($this->number->add_number_to_user($username, $id, $this->session->userdata('logged_in')['token']), true);
            if (isset($account_res)) {
                $this->session->set_flashdata('success', "Add Number Successful");
            } else {
                $this->session->set_flashdata('error', "Database error. Please try again after a while.");
            }
        }

        redirect('number/listnumbers');
    }

    /* AJAX METHODS */
    public function add_number()
    {
        $json = array(
            'status'  => 1,
            'message' => 'success',
        );

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('id', 'Account', 'trim|required|xss_clean');

        if (!$this->form_validation->run()) {
            $json['status'] = 0;
            $json['message'] = 'validation error';
        } else {
            $username = $this->input->post("username");
            $id = $this->input->post("id");
            
            $account_res = json_decode($this->number->add_number_to_user($username, $id, $this->session->userdata('logged_in')['token']), true);
            if (!isset($account_res)) {
                $json['status'] = -1;
                $json['message'] = 'server error';
            }
        }

        // set response
        echo json_encode($json);
    }

    /* AJAX METHODS */
    public function delete_number()
    {
        $json = array(
            'status'  => 0,
            'message' => 'success',
        );

        $this->form_validation->set_rules('id', 'Account', 'trim|required|xss_clean');

        if (!$this->form_validation->run()) {
            $json['status'] = 1;
            $json['message'] = 'validation error';
        } else {
            // $username = $this->input->post("username");
            $id = $this->input->post("id");
            
            $account_res = json_decode($this->number->delete_number($id, $this->session->userdata('logged_in')['token']), true);
            if (!isset($account_res)) {
                $json['status'] = -1;
                $json['message'] = 'server error';
            } else {
                $json['id'] = $account_res['id'];
            }
        }

        // set response
        echo json_encode($json);
    }
}
