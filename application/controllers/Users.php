<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *	This is the Auth controller.
 */
class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Users_model', 'users');
	}

	public function index() {
		if ($userdata = $this->session->userdata('logged_in')) {
			redirect('number/history');
		} else {
			# not logged in
			$data['title'] = "Login";
			$this->load->view('templates/html.php', $data);
			$this->load->view('contents/login.php');
			$this->load->view('templates/footer.php');
		}
	}

	public function signOut() {
		if ($this->session->userdata('logged_in'))
			$this->session->unset_userdata('logged_in');

		redirect('users');
	}

	public function listUsers() {
        $userdata = $this->session->userdata('logged_in');
        if (!isset($userdata)) {
            redirect('users');
        } else {
    		$data['title'] = "User Management";

    		$user_data['users'] = json_decode($this->users->get_user(NULL, $this->session->userdata('logged_in')), true);

            $this->load->view('templates/html.php', $data);
            $this->load->view('templates/header.php');
            $this->load->view('contents/user_management.php', $user_data);
            $this->load->view('templates/footer.php');
        }

	}

	public function addNewUser() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('full_name', 'Customer Name', 'trim|xss_clean');
		$this->form_validation->set_rules('is_admin', 'User Role', 'trim|required');

		if ($this->form_validation->run()) {
			$data['username'] = $this->input->post("username");
			$data['password'] = $this->input->post("password");
			$data['full_name'] = $this->input->post("full_name");
			$data['is_admin'] = $this->input->post("is_admin");
            $data['tariff'] = $this->input->post("tariff");

            $userdata = json_decode($this->users->insert_new_user($data, $this->session->userdata('logged_in')['token']), true);
			if (isset($userdata)) {
				$this->session->set_flashdata('success', "Add User Successful");
			} else {
				$this->session->set_flashdata('error', "Database error. Please try again after a while.");
			}

		} else {
			$this->session->set_flashdata('error', validation_errors());
		}

		redirect('users/listusers');
	}

	public function updateUser() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean');
        $this->form_validation->set_rules('is_admin', 'User Role', 'trim|required');
        $this->form_validation->set_rules('tariff', 'Tariff', 'trim|required');

		if ($this->form_validation->run()) {
            $data['username'] = $this->input->post("username");
            if ($this->input->post("password") !== NULL) {
                $data['password'] = $this->input->post("password");
            }
            $data['full_name'] = $this->input->post("full_name");
            $data['is_admin'] = $this->input->post("is_admin");
            $data['tariff'] = $this->input->post("tariff");
            $old_username = $this->input->post("old_username");
            
            $resp = $this->users->update_user_data($data, $old_username, $this->session->userdata('logged_in')['token']);
            $userdata = json_decode($resp, true);
			if (isset($userdata)) {
				$this->session->set_flashdata('success', "Edit User Successful");
			} else {
				$this->session->set_flashdata('error', "Database error. Please try again after a while.");
			}

		} else {
			$this->session->set_flashdata('error', validation_errors());
		}
		redirect('users/listusers');
	}

	public function deleteUser($id) {

        $result = json_decode($this->users->delete_user($id, $this->session->userdata('logged_in')['token']), true);
		if (isset($result)) {
			$this->session->set_flashdata('success', "Delete User Successful");
		} else {
			$this->session->set_flashdata('error', "Database error. Please try again after a while.");
		}
		redirect('users/listusers');
	}

    public function listNumbers() {
        $data['title'] = "Number Management";

        $this->load->model("Number_model", "number");

        $user_data = json_decode($this->users->get_user(NULL, $this->session->userdata('logged_in')), true);
        for ($i=0; $i < count($user_data); $i++) { 
            $user['id'] = $user_data[$i]['id'];
            $user['username'] = $user_data[$i]['username'];

            $numbers = json_decode($this->number->get_numbers($user['username'], $this->session->userdata('logged_in')['token']), true);
            $user['numbers'] = isset($numbers) ? $numbers : array();

            $numbers_data['numbers'][] = $user;
        }

        $this->load->view('templates/html.php', $data);
        $this->load->view('templates/header.php');
        $this->load->view('contents/number_management.php', $numbers_data);
        $this->load->view('templates/footer.php');
    }

    public function editNumber() {
        $new_data['id'] = $this->input->post("id");
        $method = $this->input->post("method");

        if ($method == "file") {
            $res = $this->uploadFile();
            if ($res['retval'] == -1) {
                $this->session->set_flashdata('error', $res['message']);
                redirect('users/listNumbers');
            }

            $filepath = './uploads/' . $res['filename'];
            $content = file_get_contents($filepath);
            $new_data['numbers'] = json_encode(explode(",", $content));

            // remove temporary file
            unlink($filepath);

        } else if ($method == "manual") {
            $numbers = array();

            for ($i = 1; $i <= $this->input->post('numberCount'); $i++) {
                if (!empty($this->input->post('number_' . $i)))
                    $numbers[$i-1] = $this->input->post('number_' . $i);
            }

            $new_data['numbers'] = json_encode($numbers);
        }

        if (($result_id = $this->users->update_user_data($new_data['id'], $new_data)) > 0) {
            $this->session->set_flashdata('success', "Edit Number Successful.");
        } else {
            $this->session->set_flashdata('error', "Database error. Please try again after a while.");
        }

        redirect('users/listNumbers');

    }

    public function uploadFile() {
        $config['upload_path']      = './uploads/';
        $config['allowed_types']    = 'txt';
        $config['max_size']         = 100;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('number_file')) {
            return array(
                'retval'    => -1,
                'message'   => $this->upload->display_errors(),
            );
        } else {
            return array(
                'retval'    => 1,
                'filename'  => $this->upload->data()['file_name'],
            );
        }
        
    }
}
