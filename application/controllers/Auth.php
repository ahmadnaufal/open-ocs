<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  This is the Auth controller.
 */
class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('auth_model', 'auth');
    }

    public function authorize() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            $this->load->model('users_model', 'users');
            $userdata = json_decode($this->users->get_user($username), true);
            if (isset($userdata)) {
                $auth_param = array(
                    'response_type' => 'token',
                    'client_id' => 'open-ocs',
                );

                if ($userdata['is_admin'] == 1) {
                    $res = $this->auth->authorize_token($username, $password, $auth_param, "admin");
                } else {
                    $res = $this->auth->authorize_token($username, $password, $auth_param, NULL);
                }

                $tmp = parse_url($res, PHP_URL_FRAGMENT);
                if (!isset($tmp)) {
                    $tmp = parse_url($res, PHP_URL_QUERY);
                }

                $param = array();
                parse_str($tmp, $param);
                if (array_key_exists('access_token', $param)) {
                    unset($userdata['password']);
                    $userdata['token'] = $param['access_token'];
                    $this->session->set_userdata('logged_in', $userdata);
                } else {
                    $this->session->set_flashdata('error', $param['error_description']);
                }
            } else {
                $this->session->set_flashdata('error', "Your username is unregistered. Please contact administrator for user add request.");
            }

        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        redirect('users');
    }

    public function signOut() {
        if ($this->session->userdata('logged_in'))
            $this->session->unset_userdata('logged_in');

        redirect('users');
    }

    public function listUsers() {
        $data['title'] = "User Management";

        $user_data['users'] = $this->users->get_all_users();
        for ($i=0; $i < count($user_data['users']); $i++) { 
            $user_data['users'][$i]['assigned_number'] = 0;
        }

        $this->load->view('templates/html.php', $data);
        $this->load->view('templates/header.php');
        $this->load->view('contents/user_management.php', $user_data);
        $this->load->view('templates/footer.php');
    }

    public function addNewUser() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('customer_name', 'Customer Name', 'trim|xss_clean');
        $this->form_validation->set_rules('role', 'User Role', 'trim|required');

        if ($this->form_validation->run()) {
            $data['username'] = $this->input->post("username");
            $data['password'] = hash('sha512', $this->input->post("password"));
            $data['customer_name'] = $this->input->post("customer_name");
            $data['role'] = $this->input->post("role");

            if ($result_id = $this->users->insert_new_user($data)) {
                $this->session->set_flashdata('success', "Add User Successful");
            } else {
                $this->session->set_flashdata('error', "Database error. Please try again after a while.");
            }

        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        redirect('users/listusers');
    }

    public function updateUser() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean');
        $this->form_validation->set_rules('role', 'User Role', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            $data['id'] = $this->input->post("id");
            $data['username'] = $this->input->post("username");
            if (strlen($this->input->post("password")) > 0)
                $data['password'] = hash('sha512', $this->input->post("password"));
            $data['role'] = $this->input->post("role");

            if (($result_id = $this->users->update_user_data($data['id'], $data)) > 0) {
                $this->session->set_flashdata('success', "Edit User Successful");
            } else {
                $this->session->set_flashdata('error', "Database error. Please try again after a while.");
            }

        } else {
            $this->session->set_flashdata('error', validation_errors());
        }
        redirect('users/listusers');
    }

}
