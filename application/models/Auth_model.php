<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
    

class Auth_model extends CI_Model {

    const AUTHORIZE_URL = "http://auth.ocs/authorize.php";
    
    function __construct() {
        parent::__construct();
    }

    public function authorize_token($username, $password, $data, $scope) {
        $url = self::AUTHORIZE_URL . "?";

        foreach ($data as $key => $value) {
            $url .= $key . "=" . htmlspecialchars($value) . "&";
        }
        $url = rtrim($url, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=" . htmlspecialchars($username) . "&password=" . htmlspecialchars($password) . "&scope=" .$scope);
        $result = curl_exec($ch);

        $url_res = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
        curl_close($ch);  

        return $url_res;
    }

    public function get_history_by_type($account, $type) {
        # get history by type
    }

}


?>
