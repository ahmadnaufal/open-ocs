<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
    

class History_model extends CI_Model {

    const HISTORY_URL = "http://10.14.35.164/api/v1/sessions";
    const TARIFF_URL = "http://10.14.35.164/api/v1/tariff";
    
    function __construct() {
        parent::__construct();
    }

    public function get_all_history($data) {
        $url = self::HISTORY_URL . "?";

        foreach ($data as $key => $value) {
            $url .= $key . "=" . htmlspecialchars($value) . "&";
        }
        $url = rtrim($url, '&');

        $headers = array(
            'Content-Type:application/json',
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
    }

    public function get_history_by_type($account, $type) {
        # get history by type
    }

    public function get_tariff($token) {
        $url = self::TARIFF_URL . "?access_token=" . $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
        
    }

}


?>
