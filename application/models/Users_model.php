<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	

class Users_model extends CI_Model {

	const USERS_URL = "http://api.ocs/api/v1/users";
	
	function __construct() {
		parent::__construct();
	}

	public function insert_new_user($data, $token) {
		$url = self::USERS_URL . "?access_token=" . $token;

		$query = "";
		foreach ($data as $key => $value) {
            $query .= $key . "=" . htmlspecialchars($value) . "&";
        }
        $query = rtrim($query, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
	}

	public function get_all_users() {
		return $this->db->get(TABLE_USERS)->result_array();
	}

	public function get_user_by_id($id) {
		// $this->db->from(TABLE_USERS);
		// $this->db->where('id', $id);
		// return $this->db->get()->row_array();
	}

	public function get_users_by_role($role) {
		$this->db->from(TABLE_USERS);
		$this->db->where('role', $role);
		return $this->db->get()->result_array();
	}

	public function get_user($username, $data) {
		$url = self::USERS_URL;

		if (isset($username)) {
			// Get by specific username
			$url .= "/" . $username;
		} else {
			// Get all users. Must be a privileged admin to access this.
			$url .= "?access_token=" . $data['token'];
		}

        $headers = array(
            'Content-Type:application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
	}

	public function update_user_data($data, $username, $token) {
        $url = self::USERS_URL . "/" . $username . "?access_token=" . $token;

        $query = "";
        foreach ($data as $key => $value) {
            $query .= $key . "=" . htmlspecialchars($value) . "&";
        }
        $query = rtrim($query, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
	}

	public function update_login($id) {
		$this->db->where('id', $id);
		$result = $this->db->update(TABLE_USERS, array(
			'last_login' => date("Y-m-d H:i:s"),
		));
		return $this->db->affected_rows();
	}

	public function delete_user($id, $token) {
		$url = self::USERS_URL . "/" . $id . "?access_token=" . $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
	}

}


?>
