<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
    

class Number_model extends CI_Model {

    const ACCOUNTS_URL = "http://api.ocs/api/v1/accounts";
    const ACCOUNTS_ADMIN_URL = "http://api.ocs/api/v1/numbers";
    
    function __construct() {
        parent::__construct();
    }

    public function add_number_to_user($username, $id, $token) {
        $url = self::ACCOUNTS_URL . "?access_token=" . $token;

        $query = "username=" . htmlspecialchars($username) . "&id=" . htmlspecialchars($id);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function get_numbers($username = "", $token) {
        if ($username == "") {
            $url = self::ACCOUNTS_ADMIN_URL . "?access_token=" . $token;
        } else {
            $url = self::ACCOUNTS_ADMIN_URL . "/" . $username . "?access_token=" . $token;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
    }

    public function get_number_and_user($number, $username) {
        $this->db->from(TABLE_NUMBERS);
        $this->db->where('number', $number);
        $this->db->where('username', $username);
        return $this->db->get()->row_array();
    }

    public function delete_number($id, $token) {
        $url = self::ACCOUNTS_URL . "/" . $id . "?access_token=" . $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
    }

    public function get_number_from_user($token) {
        $url = self::ACCOUNTS_URL . "?" . "access_token=" . $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec($ch);
        curl_close($ch);  

        return $result;
    }

}


?>
