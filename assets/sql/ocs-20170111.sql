-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2017 at 05:35 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ocs`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(3) NOT NULL,
  `username` varchar(24) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL DEFAULT '1',
  `customer_name` varchar(64) NOT NULL,
  `numbers` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `customer_name`, `numbers`, `last_login`) VALUES
(1, 'admin', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 0, '', NULL, '2017-01-10 21:48:41'),
(2, 'defaultuser', 'd35a7ca3bf263eaa554e4447149c892ee56d49449f5035b15dca4bbc0c5118c9541ad44f911ac61837ef9a380d3b63358b75bb8c2826d9b9ff5c3862bec5de0c', 1, 'PT Default User', NULL, '2016-10-24 00:43:08'),
(3, 'ahmadnaufal', '30da123a9be4eb625f026e300a9b5d01a342da55266890150711f26983ec4833e8221c270ea97fc65268658581efafd6107f38373db057790facb05599b6a23b', 1, 'Ahamd', NULL, NULL),
(4, 'adminlain', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 0, '', NULL, NULL),
(5, 'testuser', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', 1, 'test', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_number`
--

CREATE TABLE `users_number` (
  `number` varchar(16) NOT NULL,
  `username` varchar(24) NOT NULL,
  `status` int(1) NOT NULL,
  `time_added` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_number`
--

INSERT INTO `users_number` (`number`, `username`, `status`, `time_added`) VALUES
('089213', 'defaultuser', 1, '2016-12-07 17:00:00'),
('12131', 'defaultuser', 1, '2016-12-14 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_number`
--
ALTER TABLE `users_number`
  ADD PRIMARY KEY (`number`,`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
