function numberInputBuilder(x, value) {
    var allWrapper = $("<div id=\"numbergroup_" + x + "\">");
    var colInputWrapper = $("<div class=\"col s8\">");
    var inputGroupWrapper = $("<div class=\"input-field\">");
    var numberInput = "<input type=\"text\" class=\"validate\" id=\"number_" + x + "\" name=\"number_" + x + "\" value=\"" + value + "\">";
    var numberLabel = "<label for=\"number_" + x + "\">Number " + x + "</label>";
    inputGroupWrapper.append(numberInput, numberLabel);

    var colButtonWrapper = $("<div class=\"col s4\">");
    var buttonGroupWrapper = $("<div id=\"pull-right\">");
    var removeButton = "<button class=\"btn remove-number\" data-number-id=\"" + x + "\" type=\"button\">-</button>";
    buttonGroupWrapper.append(removeButton);

    colInputWrapper.append(inputGroupWrapper);
    colButtonWrapper.append(buttonGroupWrapper);
    allWrapper.append(colInputWrapper, colButtonWrapper);

    return allWrapper;
}

function numberAddBuilder(x) {
    var allWrapper = $("<div id=\"numbergroup_" + x + "\">");
    var colInputWrapper = $("<div class=\"col s8\">");
    var inputGroupWrapper = $("<div class=\"input-field\">");
    var numberInput = "<input type=\"text\" class=\"validate\" id=\"number_" + x + "\" name=\"number_" + x + "\" value=\"\">";
    var numberLabel = "<label for=\"number_" + x + "\">Add Another Number</label>";
    inputGroupWrapper.append(numberInput, numberLabel);

    var colButtonWrapper = $("<div class=\"col s4\">");
    var buttonGroupWrapper = $("<div id=\"pull-right\">");
    var addButton = "<button class=\"btn add-number\" data-number-id=\"" + x + "\" type=\"button\">+</button>";
    buttonGroupWrapper.append(addButton);

    colInputWrapper.append(inputGroupWrapper);
    colButtonWrapper.append(buttonGroupWrapper);
    allWrapper.append(colInputWrapper, colButtonWrapper);

    return allWrapper;
}

$("#edit_numbers").on('click', '.add-number', function() {

    $('#numberCount').val( function(i, oldval) {
        return ++oldval;
    });
    var x = $(this).attr('data-number-id');
    console.log(x);
    var user_id = $("#number_" + x).val();
    var user_username = $("#edit_numbers #username").val();

    console.log(user_id + " " + user_username);

    // do AJAX
    $.ajax({
        url: base_url + "/number/add_number",
        data: {
            format: 'json',
            id: user_id,
            username: user_username
        },
        context: {
            id: $("#number_" + x).val()
        },
        dataType: 'json',
        error: function(err) {
            alert('An error occurred. Please try again later.');
            console.log(err);
        },
        success: function(data) {
            // body...
            console.log(data);
            if (data.status == 0) {
                alert(data.message);
            } else {
                var id = data.id;
                var new_x = parseInt(x) + 1;
                var inputGroupWrapper = numberAddBuilder(new_x, "");
                $("#numberForm").append(inputGroupWrapper);

                alert(data.message);
            }
        },
        type: 'POST'
    });

    return false;
});

$("#edit_numbers").on('click', '.remove-number', function() {
    // var x = $("#numberCount").val();

    var x = $(this).attr('data-number-id');

    // do AJAX
    $.ajax({
        url: base_url + "/number/delete_number",
        data: {
            format: 'json',
            id: $("#number_" + x).val()
        },
        dataType: 'json',
        error: function(err) {
            alert('An error occurred. Please try again later.');
            console.log(err);
        },
        success: function(data) {
            // body...
            console.log(data);
            if (data.status != 0) {
                alert(data.message);
            } else {
                if ( x > 0 ) {
                    //var old_x = $("#numberCount").val(); 
                    $('#numberCount').val( function(i, oldval) {
                        return --oldval;
                    });

                    //var new_x = parseInt(x) + 1;
                    $("#numbergroup_" + x).remove();
                    //$("#numbergroup_" + new_x).remove();
                    //var inputGroupWrapper = numberAddBuilder(x, "");
                    //$("#numberForm").append(inputGroupWrapper);
                }

                alert(data.message);
            }
        },
        type: 'POST'
    });

    return false;
});

$(document).ready(function() {
    $(".button-collapse").sideNav();

    $('select').material_select();

    $('.modal').modal();

    $("#add_user").on('click', function() {
        $("#add_users #username").val("");
        $("#add_users #customer_name").val("");
        $("#add_users #user_id").val("");
        $("#add_users #old_username").val("");
        $("#add_users .modal-title").html("Add New User");
        $("#add_users #user_form").attr('action', base_url + 'users/addnewuser');

        $('#add_users').modal('open');
    });

    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd'
    });

    $(".timepick-mask").mask('H0:M0', {
        translation: {
            'H': {pattern:/[0-2]/},
            'M': {pattern:/[0-5]/}
        }
    });

    $(".edit-user").on('click', function() {
        var id = $(this).attr('data-id');
        $("#add_users #username").val($("#user_" + id + " > .user-username").html());
        $("#add_users #full_name").val($("#user_" + id + " > .user-customername").html());
        $("#add_users #tariff").val($("#user_" + id + " > .user-tariff").html());
        $("#add_users #old_username").val($("#user_" + id + " > .user-username").html());
        $("#add_users .modal-title").html("Edit User");
        $("#add_users #user_id").val(id);
        $("#add_users #user_form").attr('action', base_url + 'users/updateuser');

        $('#add_users').modal('open');
    });

    $(".edit-number").on('click', function() {
        var id = $(this).attr('data-id');
        $("#edit_numbers #username").val($("#user_" + id + " > .user-username").html());
        $("#edit_numbers #user_id").val(id);
        var numbers = $("#user_" + id + " > .number-list").html().trim().split("<br>");
        console.log(numbers.length);
        if (numbers[0] == "") {
            numbers = numbers.filter(v => v != "");
        }

        $("#edit_numbers #numberForm").html(numberAddBuilder(1));

        for (var i = 0; i < numbers.length - 1; i++) {
            if (i == 0) {
                $("#edit_numbers #numberForm").html(numberInputBuilder(i+1, numbers[i]));
            } else {
                $("#edit_numbers #numberForm").append(numberInputBuilder(i+1, numbers[i]));
            }
        }
        if (numbers.length > 0) {
            $("#edit_numbers #numberForm").append(numberAddBuilder(numbers.length));
        }

        $("#edit_numbers #numberCount").val(numbers.length > 1 ? numbers.length - 1 : 0);

        $('#edit_numbers').modal('open');
    });
});

