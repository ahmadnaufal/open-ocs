var app = angular.module('sessionApp', ['ngResource', 'ui.materialize']);

app.factory('SessionResources', function ($resource) {
    var auth_header = {'Authorization':'Basic aWJyb2hpbWlzbGFtQGdtYWlsLmNvbTpwYXNzd29yZA=='};

    return $resource(history_url, {}, {
        list: {method:'GET', isArray:true},
        get: {method:'GET'},
    })
});

app.controller('SessionHistoryController', function($scope, $timeout, SessionResources) {

    $scope.types = [
        {id: "0", label:"All Calls"},
        {id: "1", label:"Outgoing Calls"},
        {id: "2", label:"Incoming Calls"},
        {id: "3", label:"Fail/Unsuccessful Calls"},
        {id: "4", label:"Incoming & Outgoing Calls"},
    ];

    $scope.sessions = [];

    $scope.account = "";
    $scope.selected_type = "1";

    $scope.is_continued = false;

    var currentTime = new Date();
    $scope.currentTime = currentTime;
    $scope.month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $scope.monthShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $scope.weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $scope.weekdaysLetter = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    $scope.today = 'Today';
    $scope.clear = 'Clear';
    $scope.close = 'Close';
    var days = 15;
    
    $scope.refresh = function() {

        if ($scope.date_start == undefined) {
            start = "-1";
        } else {
            start = $scope.date_start;
            if ($scope.time_start == undefined) {
                start += " 00:00:00";
            } else {
                start += " " + $scope.time_start + ":00";
            }
        }
        
        if ($scope.date_end == undefined) {
            end = "-1";
        } else {
            end = $scope.date_end;
            if ($scope.time_end == undefined) {
                end += " 00:00:00";
            } else {
                end += " " + $scope.time_end + ":00";
            }
        }

        //console.log(start + " " + end);

        SessionResources.list({
            "type"      : $scope.selected_type,
            "account"   : $scope.account.length > 3 ? $scope.account : $scope.selected_number,
            "start"     : start,
            "end"       : end,
        }).$promise.then(function(result) {
            if (result != $scope.sessions) {
                $scope.sessions = result;
            }
        });
    }

    $scope.set_continous = function() {
        // TO start, make sure account length is filled
        if ($scope.account.length > 3 || $scope.selected_number.length > 3){
            $scope.is_continued = true;
        } else {
            alert("Account number is required.");
        }

        return false;
    }

    setInterval(function(){
       	// console.log($scope.is_continued);
        if ($scope.is_continued) {
            $scope.refresh();
            if ($scope.time_start != undefined || $scope.time_end != undefined) {
                $scope.is_continued = false;
            }
        }
    },1000);

});

app.controller('SessionBillingController', function($scope, $timeout, SessionResources) {

    $scope.sessions = [];

    $scope.account = "";

    $scope.is_continued = false;
    $scope.row_number = 0;

    var currentTime = new Date();
    $scope.currentTime = currentTime;
    $scope.month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $scope.monthShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $scope.weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $scope.weekdaysLetter = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    $scope.today = 'Today';
    $scope.clear = 'Clear';
    $scope.close = 'Close';
    var days = 15;
    
    $scope.refresh = function() {

        if ($scope.date_start == undefined) {
            start = "-1";
        } else {
            start = $scope.date_start;
            if ($scope.time_start == undefined) {
                start += " 00:00:00";
            } else {
                start += " " + $scope.time_start + ":00";
            }
        }
        
        if ($scope.date_end == undefined) {
            end = "-1";
        } else {
            end = $scope.date_end;
            if ($scope.time_end == undefined) {
                end += " 00:00:00";
            } else {
                end += " " + $scope.time_end + ":00";
            }
        }

        //console.log(start + " " + end);

        SessionResources.list({
            "type"      : "1",
            "account"   : $scope.account.length > 3 ? $scope.account : $scope.selected_number,
            "start"     : start,
            "end"       : end,
        }).$promise.then(function(result) {
            if (result != $scope.sessions) {
                $scope.sessions = result;
                $scope.row_number = $scope.sessions.length;
            }
        });
    }

    $scope.change_state = function() {
        if ($scope.account.length > 3 || $scope.selected_number.length > 3){
            $scope.is_continued = true;
        } else {
            alert("Account number is required.");
        }

        return false;
    }

    setInterval(function(){
        //console.log($scope.is_continued);
        if ($scope.is_continued) {
            $scope.refresh();
            if ($scope.time_start != undefined || $scope.time_end != undefined) {
                $scope.is_continued = false;
            }
        }
    },1000);

});
